import React from 'react';
import { Link } from 'react-router-dom';

import './NavBar.css';

const NavBar = () => (
  <div className="navBar">
    <div className="link-container">
      <Link to="/" className="link">Home</Link>
    </div>
    <div className="link-container">
      <Link to="/post/create" className="link">Crear Post</Link>
    </div>
    <div className="link-container">
      <Link to="/login" className="link">Login</Link>
    </div>
  </div>
);

export default NavBar;
