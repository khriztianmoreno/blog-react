import React from 'react';

const Error = () => (
  <div className="page">
    <h2>:( Opps!!</h2>
    <p>Página no encontrada</p>
  </div>
);

export default Error;
