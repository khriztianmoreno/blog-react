import React from 'react';
import Proptypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Blog.css';

const Blog = (props) => {
  const {
    image,
    title,
    content,
    id,
  } = props;

  return (
    <article className="post post-large blog-single-post">
      <div className="post-image">
        <div className="owl-carousel owl-theme" data-plugin-options='{"items":1}'>
          <div>
            <div className="img-thumbnail">
              {
                image && <img className="img-responsive" src={image} alt="Img" />
              }
            </div>
          </div>
        </div>
      </div>

      <div className="post-date">
        <span className="day">10</span>
        <span className="month">Jan</span>
      </div>

      <div className="post-content">
        <h2>
          <Link to={`/post/${id}`} href="blog-post.html">
            {title}
          </Link>
        </h2>
        <p>{content}</p>
      </div>
    </article>
  );
};

Blog.defaultProps = {
  id: 0,
  title: '',
  image: '',
  content: '',
};

Blog.propTypes = {
  id: Proptypes.number,
  title: Proptypes.string,
  image: Proptypes.string,
  content: Proptypes.string,
  /*
  styles: Proptypes.shape({
    border: Proptypes.string,
    isPrivate: Proptypes.bool
  }),
  */
};

export default Blog;
