import React, { Component } from 'react';

import { createPost } from './../../utils/api';

class Create extends Component {
  constructor() {
    super();

    this.state = {
      title: '',
      body: '',
      isPrivate: false,
    };

    this.changeFormValues = this.changeFormValues.bind(this);
  }

  changeFormValues(ev) {
    const { name, value } = ev.target;
    this.setState({
      [name]: value,
    });
  }

  async savePost(ev) {
    ev.preventDefault();
    try {
      const post = this.state;
      const { status} = await createPost(post);
      if (status === 201) {
        alert('SAVED...');
      }
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={(ev) => { this.savePost(ev); }}>
          <div className="form-group">
            <span>Titulo:</span>
            <input
              type="text"
              name="title"
              className="form-control"
              placeholder="Enter TITLE"
              onChange={this.changeFormValues}
            />
          </div>
          <div className="form-group">
            <span>Body / Content</span>
            <input
              type="text"
              name="body"
              className="form-control"
              placeholder="Enter content"
              onChange={this.changeFormValues}
            />
          </div>
          <div className="form-check">
            <input
              type="checkbox"
              name="isPrivate"
              className="form-check-input"
              onChange={(ev) => { this.setState({ isPrivate: ev.target.checked }); }}
            />
            <span className="form-check-label">Private</span>
          </div>
          <button type="submit" className="btn btn-primary">Guadar</button>
        </form>
        <hr />
        <p>Titulo: {this.state.title}</p>
        <p>Body: {this.state.body}</p>
        <p>Private: {this.state.isPrivate ? 'true' : 'false'}</p>
      </div>
    );
  }
}

export default Create;
