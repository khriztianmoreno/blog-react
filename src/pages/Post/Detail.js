import React, { Component } from 'react';
import Proptypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import Blog from './../../components/Blog/Blog';

import { getSinglePost, getCommentsByPost } from './../../utils/api';

class PostDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    this.loadPost();
    this.loadCommentsByPost();
  }

  async loadPost() {
    const { id } = this.props.match.params;

    try {
      const { data } = await getSinglePost(id);
      this.setState({ data });
    } catch (error) {
      console.log(error);
    }
  }

  async loadCommentsByPost() {
    const { id } = this.props.match.params;

    try {
      const { data } = await getCommentsByPost(id);
      this.setState({ comments: data });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { data, comments } = this.state;

    return (
      <div>
        {
          data && data.title
          ? (
            <Blog
              title={data.title}
              content={data.body}
            />
          )
          : <div>Loading ...</div>
        }
        {
          comments && comments.map((comment) => (<div key={comment.id}>{comment.body}</div>))
        }
      </div>
    );
  }
}

PostDetail.defaultProps = {
  match: {},
};

PostDetail.propTypes = {
  match: Proptypes.shape,
};

export default withRouter(PostDetail);
