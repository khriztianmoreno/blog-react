import React, { Component } from 'react';

import Blog from './../components/Blog/Blog';

import { getAllPost } from './../utils/api';
// import getAllPost from './../utils/api'.getAllPost;

/*
import getUsers, { getSimple, create } from './../utils/users';
import getProducts, {
  getSimple as getSimpleProducts,
  create as createProduct,
} from './../utils/products';
import * as supplier from './../utils/supplier';
import login as authLoginUser from './../utils/auth';
*/

import './Home.css';

import logo from './../logo.svg';

class Home extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.loadData();

    supplier.create(data)
    supplier.get()
    supplier.getSimple(id)
  }

  async loadData() {
    try {
      const { data } = await getAllPost();
      this.setState({ data });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        {this.state.data &&
          this.state.data.map(post => (
            <Blog
              key={post.id}
              id={post.id}
              title={post.title}
              image={post.picture}
              content={post.body}
            />
          ))}
      </div>
    );
  }
}

export default Home;
