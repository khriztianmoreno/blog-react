import axios from 'axios';

function getUsers() {
  return axios.get('api/user');
}

export function getSimple(id) {
  return axios.get(`api/user/${id}`);
}

export function create(data) {
  return axios.post('api/user/', data);
}

export default getUsers;
