import axios from 'axios';

const BASE_URL = 'https://jsonplaceholder.typicode.com';

export function getAllPost() {
  return axios.get(`${BASE_URL}/posts/`);
}

export function getSinglePost(id) {
  return axios.get(`${BASE_URL}/posts/${id}`);
}

export function getCommentsByPost(id) {
  return axios.get(`${BASE_URL}/posts/${id}/comments`);
}

export function createPost(data) {
  return axios.post(`${BASE_URL}/posts/`, data);
}
