import axios from 'axios';

function getProducts() {
  return axios.get('api/products');
}

/**
 * 'Private' function
 */
function replaceData(data) {
  return {
    title: data.title,
    content: data.body,
  };
}

/**
 * Return only Product
 * @param {number} identification product
 */
export function getSimple(id) {
  return axios.get(`api/products/${id}`);
}

export function create(data) {
  const cleanData = replaceData(data);
  return axios.post('api/products/', cleanData);
}

export default getProducts;
