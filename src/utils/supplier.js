import axios from 'axios';

export function get() {
  return axios.get('api/supplier');
}

export function getSimple(id) {
  return axios.get(`api/supplier/${id}`);
}

export function create(data) {
  return axios.post('api/supplier/', data);
}
