import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import PostDetail from './pages/Post/Detail';
import Create from './pages/Post/Create';
import Login from './pages/Auth/Login';

import Error from './components/Error/Error';
import NavBar from './components/NavBar/NavBar';

import './App.css';

const App = () => (
  <BrowserRouter>
    <div>
      <NavBar />
      <Switch>
        <Route
          exact
          path="/"
          component={Home}
        />
        <Route
          exact
          path="/post/create"
          component={Create}
        />
        <Route
          exact
          path="/post/:id"
          render={() => <PostDetail name="Creando una webApp" />}
        />
        <Route
          exact
          path="/login"
          component={Login}
        />
        <Route component={Error} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
